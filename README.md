# PhotoAlbumApp

The purpose on this project was to create a custom gallery with media items (images and videos) taken from Android's native gallery and camera.

The application must have the abilities to do pinch-able zoom and play the videos. It also has the ability to delete the media items taken by the application but not from the Android's native gallery.

## Other libraries

- Glide 4.11.0 (https://github.com/bumptech/glide)