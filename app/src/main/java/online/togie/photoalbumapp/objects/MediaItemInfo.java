package online.togie.photoalbumapp.objects;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class MediaItemInfo implements Comparable<MediaItemInfo>, Parcelable {

    private long mMediaId = 0L;
    private final StringBuilder mMapId = new StringBuilder("");
    private final StringBuilder mMimeType = new StringBuilder("");
    private final StringBuilder mDisplayName = new StringBuilder("");
    private final StringBuilder mBucketDisplayName = new StringBuilder("");
    private final StringBuilder mDataPath = new StringBuilder("");
    private final StringBuilder mFolderPath = new StringBuilder("");
    private int mDateAdded = 0;
    private int mDateTaken = 0;
    private long mAppDateAdded = 0L;

    public static final Creator<MediaItemInfo> CREATOR = new Creator<MediaItemInfo>() {
        @Override
        public MediaItemInfo createFromParcel(Parcel in) {
            return new MediaItemInfo(in);
        }

        @Override
        public MediaItemInfo[] newArray(int size) {
            return new MediaItemInfo[size];
        }
    };


    public MediaItemInfo() {
    }

    protected MediaItemInfo(Parcel in) {
        mMediaId = in.readLong();
        mMapId.replace(0, mMapId.length(), in.readString());
        mMimeType.replace(0, mMimeType.length(), in.readString());
        mDisplayName.replace(0, mDisplayName.length(), in.readString());
        mBucketDisplayName.replace(0, mBucketDisplayName.length(), in.readString());
        mDataPath.replace(0, mDataPath.length(), in.readString());
        mFolderPath.replace(0, mFolderPath.length(), in.readString());
        mDateAdded = in.readInt();
        mDateTaken = in.readInt();
        mAppDateAdded = in.readLong();
    }

    @Override
    public String toString() {
        StringBuilder tempToString = new StringBuilder("\n--- MediaItemInto ---\n");
        tempToString.append("mMediaId: ").append(mMediaId).append("\n");
        tempToString.append("mMapId: ").append(mMapId.toString()).append("\n");
        tempToString.append("mMimeType: ").append(mMimeType.toString()).append("\n");
        tempToString.append("mDisplayName: ").append(mDisplayName.toString()).append("\n");
        tempToString.append("mBucketDisplayName: ").append(mBucketDisplayName.toString()).append("\n");
        tempToString.append("mDataPath: ").append(mDataPath.toString()).append("\n");
        tempToString.append("mFolderPath: ").append(mFolderPath.toString()).append("\n");
        tempToString.append("mDateAdded: ").append(mDateAdded).append("\n");
        tempToString.append("mDateTaken: ").append(mDateTaken).append("\n");
        tempToString.append("mAppDateAdded: ").append(mAppDateAdded).append("\n");
        return tempToString.toString();
    }

    @Override
    public int compareTo(MediaItemInfo o) {
        if ((getAppDateAdded() > 0) && (o.getAppDateAdded() > 0)) {
            return (int) (o.getAppDateAdded() - getAppDateAdded());
        } else if ((getDateAdded() > 0) && (o.getDateAdded() > 0)) {
            return o.getDateAdded() - getDateAdded();
        } else if ((getDateTaken() > 0) && (o.getDateTaken() > 0)) {
            return o.getDateTaken() - getDateTaken();
        } else {
            return 0;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mMediaId);
        dest.writeString(mMapId.toString());
        dest.writeString(mMimeType.toString());
        dest.writeString(mDisplayName.toString());
        dest.writeString(mBucketDisplayName.toString());
        dest.writeString(mDataPath.toString());
        dest.writeString(mFolderPath.toString());
        dest.writeInt(mDateAdded);
        dest.writeInt(mDateTaken);
        dest.writeLong(mAppDateAdded);
    }

    public long getMediaId() {
        return mMediaId;
    }

    public void setMediaId(long mediaId) {
        mMediaId = mediaId;
    }

    public String getMapId() {
        return mMapId.toString();
    }

    public void setMapId(String mapId) {
        mMapId.replace(0, mMapId.length(), mapId);
    }

    public String getMimeType() {
        return mMimeType.toString();
    }

    public void setMimeType(String mimeType) {
        mMimeType.replace(0, mMimeType.length(), mimeType);
    }

    public String getDisplayName() {
        return mDisplayName.toString();
    }

    public void setDisplayName(String displayName) {
        mDisplayName.replace(0, mDisplayName.length(), displayName);
    }

    public String getBucketDisplayName() {
        return mBucketDisplayName.toString();
    }

    public void setBucketDisplayName(String bucketDisplayName) {
        mBucketDisplayName.replace(0, mBucketDisplayName.length(), !TextUtils.isEmpty(bucketDisplayName) ? bucketDisplayName : "");
    }

    public String getDataPath() {
        return mDataPath.toString();
    }

    public void setDataPath(String dataPath) {
        mDataPath.replace(0, mDataPath.length(), dataPath);
    }

    public String getFolderPath() {
        return mFolderPath.toString();
    }

    public void setFolderPath(String dataPath, String bucketName) {
        mFolderPath.replace(0, mFolderPath.length(), dataPath.substring(0, dataPath.lastIndexOf(bucketName.concat("/")))).append(bucketName).append('/');
    }

    public int getDateAdded() {
        return mDateAdded;
    }

    public void setDateAdded(int dateAdded) {
        mDateAdded = dateAdded;
    }

    public int getDateTaken() {
        return mDateTaken;
    }

    public void setDateTaken(int dateTaken) {
        mDateTaken = dateTaken;
    }

    public long getAppDateAdded() {
        return mAppDateAdded;
    }

    public void setAppDateAdded(long appDateAdded) {
        mAppDateAdded = appDateAdded;
    }
}
