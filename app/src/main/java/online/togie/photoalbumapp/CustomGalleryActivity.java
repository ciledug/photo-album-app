package online.togie.photoalbumapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import online.togie.photoalbumapp.adapters.MediaGridListAdapter;
import online.togie.photoalbumapp.fragments.DialogConfirmationFragment;
import online.togie.photoalbumapp.objects.MediaItemInfo;
import online.togie.photoalbumapp.utils.AppConstants;

public class CustomGalleryActivity extends AppCompatActivity {

    private final String TAG = CustomGalleryActivity.class.getSimpleName();

    private DialogFragment mDialogConfirmationFragment;

    private ImageView mNoDataImage;
    private GridView mMediaListContainer;
    private MediaGridListAdapter mMediaAdapter;

    private Map<String, MediaItemInfo> mMediaItemList = new HashMap<>();

    private MediaGridListAdapter.HomeScreenMediaAdapterListener mMediaListListener = new MediaGridListAdapter.HomeScreenMediaAdapterListener() {
        @Override
        public void onItemClicked(MediaItemInfo mii) {
            showDialogConfirmation(mii, AppConstants.OPERATION_TYPE_PICK_UP, true, getString(R.string.dialog_text_question_pick_media));
        }

        @Override
        public void onItemLongClicked(MediaItemInfo mii) {
        }
    };

    private DialogConfirmationFragment.DialogConfirmationFragmentListener mConfirmationDialogListener = new DialogConfirmationFragment.DialogConfirmationFragmentListener() {
        @Override
        public void onConfirmationOkButtonClicked(MediaItemInfo mii, int sourceFromType) {
            mDialogConfirmationFragment.dismiss();
            pickSelectedMediaItemInfo(mii);
        }

        @Override
        public void onConfirmationCancelButtonClicked() {
            mDialogConfirmationFragment.dismiss();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_gallery);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mNoDataImage = findViewById(R.id.custom_gallery_no_data_image);

        mMediaAdapter = new MediaGridListAdapter(getApplicationContext(), mMediaListListener);
        mMediaListContainer = findViewById(R.id.custom_gallery_list_container);
        mMediaListContainer.setNumColumns(getPossibleColumnsCount());
        mMediaListContainer.setAdapter(mMediaAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNativeGalleryItems();
    }

    private int getPossibleColumnsCount() {
        DisplayMetrics dispMetrics = getApplicationContext().getResources().getDisplayMetrics();
        float tempWidth = dispMetrics.widthPixels / dispMetrics.density;
        return (int) (tempWidth / 130);
    }

    private void getNativeGalleryItems() {
        for (MediaItemInfo mii : getNativeGalleryItems(AppConstants.MEDIA_TYPE_IMAGE)) {
            mMediaItemList.put(mii.getMapId(), mii);
        }

        for (MediaItemInfo mii : getNativeGalleryItems(AppConstants.MEDIA_TYPE_VIDEO)) {
            mMediaItemList.put(mii.getMapId(), mii);
        }

        mMediaAdapter.populateMediaItems(mMediaItemList);
        showMediaListContainer();
    }

    private List<MediaItemInfo> getNativeGalleryItems(int mediaTypeCode) {
        String colMediaId = MediaStore.MediaColumns._ID;
        String colMediaData = MediaStore.MediaColumns.DATA;
        String colMimeType = MediaStore.MediaColumns.MIME_TYPE;
        String colDisplayName = MediaStore.MediaColumns.DISPLAY_NAME;
        String colBucketDisplayName = MediaStore.MediaColumns.BUCKET_DISPLAY_NAME;
        String colBucketId = MediaStore.MediaColumns.BUCKET_ID;
        String colDateAdded = MediaStore.MediaColumns.DATE_ADDED;
        String colDateTaken = MediaStore.MediaColumns.DATE_TAKEN;

        Uri allMediaUris = null;
        String[] projection = new String[] {
                colMediaId, colMediaData, colMimeType, colDisplayName, colBucketDisplayName, colBucketId, colDateAdded, colDateTaken
        };

        switch (mediaTypeCode) {
            case AppConstants.MEDIA_TYPE_VIDEO:
                allMediaUris = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                break;

            case AppConstants.MEDIA_TYPE_IMAGE:
            default:
                allMediaUris = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                break;
        }

        List<MediaItemInfo> tempMediaList = new ArrayList<>();
        Cursor cursor = this.getContentResolver().query(allMediaUris, projection, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();

            while (cursor.moveToNext()) {
                try {
                    MediaItemInfo mii = new MediaItemInfo();
                    mii.setMediaId(cursor.getLong(cursor.getColumnIndexOrThrow(colMediaId)));
                    mii.setMimeType(cursor.getString(cursor.getColumnIndexOrThrow(colMimeType)));
                    mii.setBucketDisplayName(cursor.getString(cursor.getColumnIndexOrThrow(colBucketDisplayName)));
                    mii.setDisplayName(cursor.getString(cursor.getColumnIndexOrThrow(colDisplayName)));
                    mii.setDataPath(cursor.getString(cursor.getColumnIndexOrThrow(colMediaData)));
                    mii.setFolderPath(mii.getDataPath(), mii.getBucketDisplayName());
                    mii.setDateAdded(cursor.getInt(cursor.getColumnIndexOrThrow(colDateAdded)));
                    mii.setDateTaken(cursor.getInt(cursor.getColumnIndexOrThrow(colDateTaken)));
                    mii.setMapId(mii.getDateAdded() + "_" + mii.getMediaId());
                    tempMediaList.add(mii);
                } catch (IllegalArgumentException ex) {
                    Log.e(TAG + ".getNativeGalleryItems", "IllegalArgumentException: " + ex.getMessage());
                }
            }

            cursor.close();
        }

        return tempMediaList;
    }

    private void showMediaListContainer() {
        mNoDataImage.setVisibility((mMediaAdapter.getCount() > 0) ? View.GONE : View.VISIBLE);
    }

    private void showDialogConfirmation(MediaItemInfo mii, int sourceFromType, boolean isWithImage, String message) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment tempFragment = getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_dialog_confirmation_fragment));
        if (null != tempFragment) {
            ft.remove(tempFragment);
        }
        ft.addToBackStack(null);

        mDialogConfirmationFragment = DialogConfirmationFragment.newInstance(mConfirmationDialogListener, mii, sourceFromType, isWithImage, message);
        mDialogConfirmationFragment.show(ft, getString(R.string.tag_dialog_confirmation_fragment));
    }

    private void pickSelectedMediaItemInfo(MediaItemInfo mii) {
        Calendar calendar = Calendar.getInstance();
        mii.setAppDateAdded(calendar.getTimeInMillis());

        Intent i = new Intent();
        i.putExtra(getString(R.string.intent_extra_selected_media_item), mii);
        setResult(Activity.RESULT_OK, i);
        finish();
    }

//    private void getMediaList(String[] mimeTypes) {
//        Map<String, MediaItemInfo> tempList = new HashMap<>();
//        Set set = mMediaItemList.entrySet();
//        Iterator iterator = set.iterator();
//
//        while (iterator.hasNext()) {
//            Map.Entry mapEntry = (Map.Entry) iterator.next();
//            MediaItemInfo mii = (MediaItemInfo) mapEntry.getValue();
//
//            if (Arrays.asList(mimeTypes).contains(mii.getMimeType())) {
//                tempList.put((String) mapEntry.getKey(), mii);
//            }
//        }
//
//        mMediaAdapter.populateMediaItems(tempList);
//        if (mMediaAdapter.getCount() > 0) {
//            showMediaListContainer(true);
//        } else {
//            showMediaListContainer(false);
//        }
//    }


//    private boolean deleteSelectedMediaItem(MediaItemInfo mii) {
//        boolean processResult = false;
//        Uri localUri = null;
//
//        switch (mii.getMimeType()) {
//            case "image/jpeg":
//            case "image/jpg":
//                localUri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                break;
//            case "video/mp4": localUri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI; break;
//            default: break;
//        }
//
//        ContentResolver cr = getApplicationContext().getContentResolver();
//        if (cr.delete(localUri, "_ID=?", new String[] { String.valueOf(mii.getMediaId()) }) > 0) {
//            processResult = true;
//        }
//
//        return processResult;
//    }

}
