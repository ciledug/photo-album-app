package online.togie.photoalbumapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;

import online.togie.photoalbumapp.objects.MediaItemInfo;

public class ImageDetailActivity extends AppCompatActivity {

    private final String TAG = ImageDetailActivity.class.getSimpleName();

    private ImageView mDetailImage;
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;

    private ScaleGestureDetector.OnScaleGestureListener mGestureListener = new ScaleGestureDetector.OnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
            setImageScale();
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            setImageScale();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDetailImage = findViewById(R.id.detail_image_view);
        mScaleGestureDetector = new ScaleGestureDetector(getApplicationContext(), mGestureListener);

        Intent i = getIntent();
        MediaItemInfo mii = i.getParcelableExtra(getString(R.string.intent_extra_selected_media_item));

        showPicture(mii);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleGestureDetector.onTouchEvent(event);
        return true;
    }

    private void showPicture(MediaItemInfo mii) {
        Glide.with(getApplicationContext())
                .load(mii.getDataPath())
                .placeholder(R.mipmap.zzz_no_image_available)
                .into(mDetailImage);
    }

    private void setImageScale() {
        if (mScaleFactor < 1.0f) { mScaleFactor = 1.0f; }
        else if (mScaleFactor > 10.0f) { mScaleFactor = 10.0f; }
        mDetailImage.setScaleX(mScaleFactor);
        mDetailImage.setScaleY(mScaleFactor);
    }
}
