package online.togie.photoalbumapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public static String DB_NAME = "online.togie.photoalbumapp.db";

    private final String CREATE_TABLE_MEDIA_ITEM =
            "CREATE TABLE " + MediaItemTable.TABLE_NAME + " (" +
            MediaItemTable.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            MediaItemTable.COLUMN_MEDIA_ID + " INTEGER, " +
            MediaItemTable.COLUMN_MAP_ID + " TEXT UNIQUE, " +
            MediaItemTable.COLUMN_MIME_TYPE + " TEXT, " +
            MediaItemTable.COLUMN_DISPLAY_NAME + " TEXT, " +
            MediaItemTable.COLUMN_BUCKET_DISPLAY_NAME + " TEXT, " +
            MediaItemTable.COLUMN_DATA_PATH + " TEXT, " +
            MediaItemTable.COLUMN_FOLDER_PATH + " TEXT, " +
            MediaItemTable.COLUMN_DATE_ADDED + " INTEGER, " +
            MediaItemTable.COLUMN_DATE_TAKEN + " INTEGER, " +
            MediaItemTable.COLUMN_APP_DATE_ADDED + " INTEGER " +
            ")";

    private final String DROP_TABLE_MEDIA_ITEM = "DROP TABLE IF EXISTS " + MediaItemTable.TABLE_NAME;

    public DBConnection(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MEDIA_ITEM);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_MEDIA_ITEM);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
