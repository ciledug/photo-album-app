package online.togie.photoalbumapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import online.togie.photoalbumapp.objects.MediaItemInfo;

public class MediaItemTable extends DBConnection {

    private final String TAG = getClass().getSimpleName();

    public static final String TABLE_NAME = "media_item";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_MEDIA_ID = "media_id";
    public static final String COLUMN_MAP_ID = "map_id";
    public static final String COLUMN_MIME_TYPE = "mime_type";
    public static final String COLUMN_DISPLAY_NAME = "display_name";
    public static final String COLUMN_BUCKET_DISPLAY_NAME = "bucket_display_name";
    public static final String COLUMN_DATA_PATH = "data_path";
    public static final String COLUMN_FOLDER_PATH = "folder_path";
    public static final String COLUMN_DATE_ADDED = "date_added";
    public static final String COLUMN_DATE_TAKEN = "date_taken";
    public static final String COLUMN_APP_DATE_ADDED = "app_date_added";


    public MediaItemTable(Context context) {
        super(context);
    }

    public List<MediaItemInfo> getAllMediaItems() {
        List<MediaItemInfo> tempList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] projection = {
            COLUMN_ID, COLUMN_MEDIA_ID, COLUMN_MAP_ID, COLUMN_MIME_TYPE, COLUMN_DISPLAY_NAME, COLUMN_BUCKET_DISPLAY_NAME,
            COLUMN_DATA_PATH, COLUMN_FOLDER_PATH, COLUMN_DATE_ADDED, COLUMN_DATE_TAKEN, COLUMN_APP_DATE_ADDED
        };

        Cursor cursor = db.query(TABLE_NAME, projection, null, null, null, null, COLUMN_APP_DATE_ADDED + " DESC");

        while (cursor.moveToNext()) {
            MediaItemInfo mii = new MediaItemInfo();

            try {
                mii.setMediaId(cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_MEDIA_ID)));
                mii.setMapId(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MAP_ID)));
                mii.setMimeType(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MIME_TYPE)));
                mii.setDisplayName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DISPLAY_NAME)));
                mii.setBucketDisplayName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_BUCKET_DISPLAY_NAME)));
                mii.setDataPath(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATA_PATH)));
                mii.setDateAdded(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_DATE_ADDED)));
                mii.setDateTaken(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_DATE_TAKEN)));
                mii.setAppDateAdded(cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_APP_DATE_ADDED)));
                mii.setFolderPath(mii.getDataPath(), mii.getBucketDisplayName());
                tempList.add(mii);

            } catch (IllegalArgumentException ex) {
                Log.e(TAG + ".getAllMediaItems", "IllegalArgumentException: " + ex.getMessage());
            }

            mii = null;
        }

        cursor.close();
        return tempList;
    }

    public long insertMediaItem(MediaItemInfo mii) {
        long insertId = -1L;
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_MEDIA_ID, mii.getMediaId());
        cv.put(COLUMN_MAP_ID, mii.getMapId());
        cv.put(COLUMN_MIME_TYPE, mii.getMimeType());
        cv.put(COLUMN_DISPLAY_NAME, mii.getDisplayName());
        cv.put(COLUMN_BUCKET_DISPLAY_NAME, mii.getBucketDisplayName());
        cv.put(COLUMN_DATA_PATH, mii.getDataPath());
        cv.put(COLUMN_FOLDER_PATH, mii.getFolderPath());
        cv.put(COLUMN_DATE_ADDED, mii.getDateAdded());
        cv.put(COLUMN_DATE_TAKEN, mii.getDateTaken());
        cv.put(COLUMN_APP_DATE_ADDED, mii.getAppDateAdded());

        try {
            insertId =  db.insert(TABLE_NAME, null, cv);
        } catch (SQLiteConstraintException ex) {
            Log.e(TAG + ".insertMediaItem", "SQLiteConstraintException: " + ex.getMessage());
        }
        return insertId;
    }

    public boolean removeMediaItem(MediaItemInfo mii) {
        SQLiteDatabase db = getWritableDatabase();
        int deletedCount = db.delete(TABLE_NAME, COLUMN_MAP_ID + "=?", new String[] { mii.getMapId() });
        return (deletedCount > -1) ? true : false;
    }
}
