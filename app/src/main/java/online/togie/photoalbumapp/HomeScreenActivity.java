package online.togie.photoalbumapp;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import online.togie.photoalbumapp.adapters.MediaGridListAdapter;
import online.togie.photoalbumapp.database.DBConnection;
import online.togie.photoalbumapp.database.MediaItemTable;
import online.togie.photoalbumapp.fragments.DialogCameraChoicesFragment;
import online.togie.photoalbumapp.fragments.DialogConfirmationFragment;
import online.togie.photoalbumapp.fragments.DialogNotificationFragment;
import online.togie.photoalbumapp.objects.MediaItemInfo;
import online.togie.photoalbumapp.utils.AppConstants;

public class HomeScreenActivity extends AppCompatActivity {

    private final String TAG = HomeScreenActivity.class.getSimpleName();
    private final int WRITE_EXTERNAL_STORAGE_REQ_CODE = 1;
    private final int CAMERA_REQ_CODE = 2;
    private final String CAMERA_BUCKET_DISPLAY_NAME = Environment.DIRECTORY_DCIM;

    private FloatingActionButton mMainFab;
    private FloatingActionButton mCameraFab;
    private FloatingActionButton mGalleryFab;

    private DialogFragment mDialogConfirmationFragment;
    private DialogFragment mDialogNotificationFragment;
    private BottomSheetDialogFragment mDialogCameraChoicesFragment;

    private ImageView mNoDataImage;
    private GridView mMediaListContainer;
    private MediaGridListAdapter mMediaAdapter;
    private Map<String, MediaItemInfo> mMediaItemList = new HashMap<>();

    private boolean mIsSubFabShown = false;
    private float mMainFabRotationDegree = 0.0f;

    private StringBuilder mRequestedPermission = new StringBuilder(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    private StringBuilder mRequestPermissionMessage = new StringBuilder("");
    private int mRequestedPermissionCode = 0;

    private DBConnection mDbConnection;
    private MediaItemTable mMediaItemTable;

    private Uri mCameraImageUri;
    private File mCameraImageFile;
    private StringBuilder mCameraMimeType = new StringBuilder("");

    private MediaGridListAdapter.HomeScreenMediaAdapterListener mMediaListListener = new MediaGridListAdapter.HomeScreenMediaAdapterListener() {
        @Override
        public void onItemClicked(MediaItemInfo mii) {
            gotoDetailPage(mii);
        }

        @Override
        public void onItemLongClicked(MediaItemInfo mii) {
            showDialogConfirmation(mii, AppConstants.OPERATION_TYPE_DELETE, true, getString(R.string.dialog_text_question_delete_media));
        }
    };

    private DialogNotificationFragment.DialogNotificationFragmentListener mNotificationDialogListener = new DialogNotificationFragment.DialogNotificationFragmentListener() {
        @Override
        public void onNotificationButtonClicked() {
            mDialogNotificationFragment.dismiss();
        }
    };

    private DialogConfirmationFragment.DialogConfirmationFragmentListener mConfirmationDialogListener = new DialogConfirmationFragment.DialogConfirmationFragmentListener() {
        @Override
        public void onConfirmationOkButtonClicked(MediaItemInfo mii, int sourceFromType) {
            switch (sourceFromType) {
                case AppConstants.OPERATION_TYPE_DELETE:
                    mDialogConfirmationFragment.dismiss();
                    removeMediaItem(mii);
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onConfirmationCancelButtonClicked() {
            mDialogConfirmationFragment.dismiss();
        }
    };

    private DialogCameraChoicesFragment.DialogCameraChoicesFragmentListener mDialogCameraChoicesListener = new DialogCameraChoicesFragment.DialogCameraChoicesFragmentListener() {
        @Override
        public void onCameraChoiceClicked(DialogCameraChoicesFragment.CameraChoiceInfo cci) {
            gotoNativeCamera(cci.getIntentTakeRequest());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initLocalDatabase();
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAppMediaItems();
    }

    @Override
    protected void onDestroy() {
        mDbConnection.close();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            switch (requestCode) {
                case WRITE_EXTERNAL_STORAGE_REQ_CODE: gotoCustomGallery(); break;
                case CAMERA_REQ_CODE: showCameraChoicesDialog(); break;
                default: break;
            }

        } else {
            showDialogNotification(mRequestPermissionMessage.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.INTENT_TAKE_PHOTO_REQUEST || requestCode == AppConstants.INTENT_TAKE_VIDEO_REQUEST) {
                insertCameraImage();
            } else if (requestCode == AppConstants.INTENT_PICK_GALERY_REQUEST) {
                MediaItemInfo mii = data.getParcelableExtra(getString(R.string.intent_extra_selected_media_item));
                if (mii != null) {
                    insertMediaItem(mii);
                }
            }

        } else {
            if (requestCode == AppConstants.INTENT_TAKE_PHOTO_REQUEST || requestCode == AppConstants.INTENT_TAKE_VIDEO_REQUEST) {
                removeCameraImage();
            }
        }
    }

    private void initLocalDatabase() {
        mDbConnection = new DBConnection(getApplicationContext());
        mMediaItemTable = new MediaItemTable(getApplicationContext());
    }

    private void initUI() {
        mNoDataImage = findViewById(R.id.home_screen_no_data_image);
        mCameraFab = findViewById(R.id.home_screen_fab_camera);
        mGalleryFab = findViewById(R.id.home_screen_fab_gallery);
        mMainFab = findViewById(R.id.home_screen_fab);

        mMainFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFabSubButtons();
            }
        });

        mCameraFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(Manifest.permission.CAMERA);
                showFabSubButtons();
            }
        });

        mGalleryFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                showFabSubButtons();
            }
        });

        mMediaAdapter = new MediaGridListAdapter(getApplicationContext(), mMediaListListener);
        mMediaListContainer = findViewById(R.id.home_screen_list_container);
        mMediaListContainer.setNumColumns(getPossibleColumnsCount());
        mMediaListContainer.setAdapter(mMediaAdapter);
    }

    private int getPossibleColumnsCount() {
        DisplayMetrics dispMetrics = getApplicationContext().getResources().getDisplayMetrics();
        float tempWidth = dispMetrics.widthPixels / dispMetrics.density;
        return (int) (tempWidth / 130);
    }

    private void getAppMediaItems() {
        for (MediaItemInfo mii : mMediaItemTable.getAllMediaItems()) {
            mMediaItemList.put(mii.getMapId(), mii);
        }

        mMediaAdapter.populateMediaItems(mMediaItemList);
        showMediaListContainer();
    }

    private void insertMediaItem(MediaItemInfo mii) {
        mMediaItemTable.insertMediaItem(mii);
        getAppMediaItems();
    }

    private void removeMediaItem(MediaItemInfo mii) {
        if (mMediaItemTable.removeMediaItem(mii)) {
            if (mii.getMediaId() <= 0) {
                File file = new File(mii.getDataPath());
                Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), AppConstants.APP_PACKAGE_NAME + ".fileprovider", file);
                ContentResolver cr = getContentResolver();
                if (cr.delete(fileUri, "_display_name=?", new String[] { file.getName() }) < 0) {
                    Log.e(TAG + ".removeMediaItem", "failed to delete file: " + file.getAbsolutePath());
                }
            }

            mMediaItemList.remove(mii.getMapId());
            mMediaAdapter.removeItem(mii);
            showMediaListContainer();
            Toast.makeText(getApplicationContext(), getString(R.string.toast_file_delete_success), Toast.LENGTH_SHORT).show();
        }
    }

    private void removeCameraImage() {
        if (mCameraImageFile.exists() && mCameraImageFile.isFile()) {
            mCameraImageFile.delete();
        }
    }

    private void insertCameraImage() {
        int timeStamp = Integer.parseInt(mCameraImageFile.getName()
                .substring(mCameraImageFile.getName().lastIndexOf("_") + 1, mCameraImageFile.getName().lastIndexOf("_") + 10), 10);

        MediaItemInfo mii = new MediaItemInfo();
        mii.setMimeType(mCameraMimeType.toString());
        mii.setDisplayName(mCameraImageFile.getName());
        mii.setBucketDisplayName(CAMERA_BUCKET_DISPLAY_NAME);
        mii.setDataPath(mCameraImageFile.getAbsolutePath());
        mii.setFolderPath(mii.getDataPath(), mii.getBucketDisplayName());
        mii.setDateAdded(timeStamp);
        mii.setDateTaken(timeStamp);
        mii.setAppDateAdded(Calendar.getInstance().getTimeInMillis());
        mii.setMapId(mii.getDateAdded() + "_" + mii.getMediaId());
        insertMediaItem(mii);
    }

    private void showFabSubButtons() {
        if (!mIsSubFabShown) {
            mMainFabRotationDegree = 135.0f;
            mCameraFab.animate().translationY(-1 * getResources().getDimension(R.dimen.fab_shown_camera));
            mGalleryFab.animate().translationY(-1 * getResources().getDimension(R.dimen.fab_shown_gallery));
        } else {
            mMainFabRotationDegree = 0.0f;
            mCameraFab.animate().translationY(0);
            mGalleryFab.animate().translationY(0);
        }

        ViewCompat.animate(mMainFab).rotation(mMainFabRotationDegree).withLayer().setInterpolator(new OvershootInterpolator(10.0f)).start();
        mIsSubFabShown = !mIsSubFabShown;
    }

    private void checkPermission(String manifestPermission) {
        mRequestedPermission.replace(0, mRequestedPermission.length(), manifestPermission);

        switch (manifestPermission) {
            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                mRequestedPermissionCode = WRITE_EXTERNAL_STORAGE_REQ_CODE;
                mRequestPermissionMessage.replace(0, mRequestPermissionMessage.length(), getString(R.string.permission_req_storage));
                break;
            case Manifest.permission.CAMERA:
                mRequestedPermissionCode = CAMERA_REQ_CODE;
                mRequestPermissionMessage.replace(0, mRequestPermissionMessage.length(), getString(R.string.permission_req_camera));
                break;
            default:
                mRequestedPermissionCode = 0;
                mRequestPermissionMessage.delete(0, mRequestPermissionMessage.length());
                break;
        }

        if (ContextCompat.checkSelfPermission(HomeScreenActivity.this, manifestPermission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                HomeScreenActivity.this, new String[] { manifestPermission }, mRequestedPermissionCode
            );
        } else {
            switch (manifestPermission) {
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    gotoCustomGallery();
                    break;
                case Manifest.permission.CAMERA:
                    showCameraChoicesDialog();
                    break;
                default: break;
            }

        }
    }

    private void gotoCustomGallery() {
        Intent i = new Intent(getApplicationContext(), CustomGalleryActivity.class);
        startActivityForResult(i, AppConstants.INTENT_PICK_GALERY_REQUEST);
    }

    private void gotoNativeCamera(int intentCameraTake) {
        Intent i = null;
        StringBuilder fileSuffix = new StringBuilder("");

        switch (intentCameraTake) {
            case AppConstants.INTENT_TAKE_PHOTO_REQUEST:
                i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileSuffix.replace(0, fileSuffix.length(), ".jpeg");
                mCameraMimeType.replace(0, mCameraMimeType.length(), AppConstants.MIME_TYPE_IMAGE_JPEG);
                break;
            case AppConstants.INTENT_TAKE_VIDEO_REQUEST:
                i = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                fileSuffix.replace(0, fileSuffix.length(), ".mp4");
                mCameraMimeType.replace(0, mCameraMimeType.length(), AppConstants.MIME_TYPE_VIDEO_MP4);
                break;
            default: break;
        }

        if (i.resolveActivity(getPackageManager()) != null) {
            mCameraImageFile = null;

            try {
                Calendar calendar = Calendar.getInstance();

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(calendar.getTime());
                String imageName = "photoalbumapp_" + timeStamp + "_" + calendar.getTimeInMillis();
//                File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                File storageDir = getExternalFilesDir(CAMERA_BUCKET_DISPLAY_NAME);

                mCameraImageFile = File.createTempFile(imageName, fileSuffix.toString(), storageDir);
//                mImageFilePath.replace(0, mImageFilePath.length(), imageFile.getAbsolutePath());
            } catch (IOException ex) {
                Log.e(TAG + ".gotoNativeCamera", "IOException: " + ex.getMessage());
            }

            if (null != mCameraImageFile) {
                mCameraImageUri = FileProvider.getUriForFile(getApplicationContext(),
                        AppConstants.APP_PACKAGE_NAME + ".fileprovider",
                        mCameraImageFile
                );
                i.putExtra(MediaStore.EXTRA_OUTPUT, mCameraImageUri);

                startActivityForResult(i, intentCameraTake);
                mDialogCameraChoicesFragment.dismiss();
            }
        }
    }

    private void gotoDetailPage(MediaItemInfo mii) {
        Intent i;
        if (mii.getMimeType().equalsIgnoreCase(AppConstants.MIME_TYPE_VIDEO_MP4)) {
            i = new Intent(getApplicationContext(), VideoDetailActivity.class);
        } else {
            i = new Intent(getApplicationContext(), ImageDetailActivity.class);
        }
        i.putExtra(getString(R.string.intent_extra_selected_media_item), mii);
        startActivity(i);
    }

    private void showMediaListContainer() {
        mNoDataImage.setVisibility((mMediaAdapter.getCount() > 0) ? View.GONE : View.VISIBLE);
    }

    private void showDialogNotification(String message) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment tempFragment = getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_dialog_notification_fragment));
        if (null != tempFragment) {
            ft.remove(tempFragment);
        }
        ft.addToBackStack(null);

        mDialogNotificationFragment = DialogNotificationFragment.newInstance(mNotificationDialogListener, message);
        mDialogNotificationFragment.show(ft, getString(R.string.tag_dialog_notification_fragment));
    }

    private void showDialogConfirmation(MediaItemInfo mii, int sourceFromType, boolean isWithImage, String message) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment tempFragment = getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_dialog_confirmation_fragment));
        if (null != tempFragment) {
            ft.remove(tempFragment);
        }
        ft.addToBackStack(null);

        mDialogConfirmationFragment = DialogConfirmationFragment.newInstance(mConfirmationDialogListener, mii, sourceFromType, isWithImage, message);
        mDialogConfirmationFragment.show(ft, getString(R.string.tag_dialog_confirmation_fragment));
    }

    private void showCameraChoicesDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment tempFragment = getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_dialog_camera_choices_fragment));
        if (null != tempFragment) {
            ft.remove(tempFragment);
        }
        ft.addToBackStack(null);

        mDialogCameraChoicesFragment = DialogCameraChoicesFragment.newInstance(getApplicationContext(), mDialogCameraChoicesListener);
        mDialogCameraChoicesFragment.show(ft, getString(R.string.tag_dialog_camera_choices_fragment));
    }

//    private boolean deleteSelectedMediaItem(MediaItemInfo mii) {
//        boolean processResult = false;
//        Uri localUri = null;
//
//        switch (mii.getMimeType()) {
//            case "image/jpeg":
//            case "image/jpg":
//                localUri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                break;
//            case "video/mp4": localUri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI; break;
//            default: break;
//        }
//
//        ContentResolver cr = getApplicationContext().getContentResolver();
//        if (cr.delete(localUri, "_ID=?", new String[] { String.valueOf(mii.getMediaId()) }) > 0) {
//            processResult = true;
//        }
//
//        return processResult;
//    }

//    private MediaItemInfo getMediaItemInfo(Uri itemUri) {
//        MediaItemInfo tempMediaItemInfo = null;
//
//        String colMediaId = MediaStore.MediaColumns._ID;
//        String colMediaData = MediaStore.MediaColumns.DATA;
//        String colMimeType = MediaStore.MediaColumns.MIME_TYPE;
//        String colDisplayName = MediaStore.MediaColumns.DISPLAY_NAME;
//        String colBucketDisplayName = MediaStore.MediaColumns.BUCKET_DISPLAY_NAME;
//        String colBucketId = MediaStore.MediaColumns.BUCKET_ID;
//        String colDateAdded = MediaStore.MediaColumns.DATE_ADDED;
//        String colDateTaken = MediaStore.MediaColumns.DATE_TAKEN;
//
//        String[] projection = new String[] {
//                colMediaId, colMediaData, colMimeType, colDisplayName, colBucketDisplayName, colBucketId, colDateAdded, colDateTaken
//        };
//        Cursor cursor = this.getContentResolver().query(itemUri, projection, null, null, null);
//
//        if (cursor != null) {
//            cursor.moveToFirst();
//
//            try {
//                tempMediaItemInfo = new MediaItemInfo();
//                tempMediaItemInfo.setMediaId(cursor.getLong(cursor.getColumnIndexOrThrow(colMediaId)));
//                tempMediaItemInfo.setMimeType(cursor.getString(cursor.getColumnIndexOrThrow(colMimeType)));
//                tempMediaItemInfo.setBucketDisplayName(cursor.getString(cursor.getColumnIndexOrThrow(colBucketDisplayName)));
//                tempMediaItemInfo.setDisplayName(cursor.getString(cursor.getColumnIndexOrThrow(colDisplayName)));
//                tempMediaItemInfo.setDataPath(cursor.getString(cursor.getColumnIndexOrThrow(colMediaData)));
//                tempMediaItemInfo.setFolderPath(tempMediaItemInfo.getDataPath(), tempMediaItemInfo.getBucketDisplayName());
//                tempMediaItemInfo.setDateAdded(cursor.getInt(cursor.getColumnIndexOrThrow(colDateAdded)));
//                tempMediaItemInfo.setDateTaken(cursor.getInt(cursor.getColumnIndexOrThrow(colDateTaken)));
//                tempMediaItemInfo.setMapId(tempMediaItemInfo.getDateAdded() + "_" + tempMediaItemInfo.getMediaId());
//
//            } catch (IllegalArgumentException ex) {
//                Log.e(TAG + ".getGalleryItemInfo", "IllegalArgumentException: " + ex.getMessage());
//            }
//
//            cursor.close();
//        } else {
//            Log.e(TAG + ".getGalleryItemInfo", "Cursor is NULL ...");
//        }
//
//        return tempMediaItemInfo;
//    }

}
