package online.togie.photoalbumapp.utils;

public class AppConstants {

    public static final String APP_PACKAGE_NAME = "online.togie.photoalbumapp";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final String MIME_TYPE_IMAGE_JPEG = "image/jpeg";
    public static final String MIME_TYPE_IMAGE_JPG = "image/jpg";
    public static final String MIME_TYPE_IMAGE_GIF = "image/gif";
    public static final String MIME_TYPE_IMAGE_PNG = "image/png";
    public static final String MIME_TYPE_VIDEO_MP4 = "video/mp4";

    public static final int OPERATION_TYPE_DELETE = 1;
    public static final int OPERATION_TYPE_PICK_UP = 2;

    public static final int INTENT_PICK_GALERY_REQUEST = 22;
    public static final int INTENT_TAKE_PHOTO_REQUEST = 23;
    public static final int INTENT_TAKE_VIDEO_REQUEST = 24;
}
