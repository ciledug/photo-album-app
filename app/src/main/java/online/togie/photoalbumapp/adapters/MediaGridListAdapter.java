package online.togie.photoalbumapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import online.togie.photoalbumapp.R;
import online.togie.photoalbumapp.objects.MediaItemInfo;
import online.togie.photoalbumapp.utils.AppConstants;

public class MediaGridListAdapter extends BaseAdapter {

    public interface HomeScreenMediaAdapterListener {
        void onItemClicked(MediaItemInfo mii);
        void onItemLongClicked(MediaItemInfo mii);
    };

    private Context mContext;
    private List<MediaItemInfo> mMediaItemList = new ArrayList<>();
    private HomeScreenMediaAdapterListener mListener;

    public MediaGridListAdapter(Context context, HomeScreenMediaAdapterListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mMediaItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMediaItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final MediaItemInfo mii = mMediaItemList.get(position);

        if (view == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.item_media, null);
        }

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onItemLongClicked(mii);
                return false;
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClicked(mii);
            }
        });

        ImageView thumbnail = view.findViewById(R.id.item_media_thumbnail);
        ImageView btnPlay = view.findViewById(R.id.item_media_btn_play);

        Glide.with(mContext)
                .load(mii.getDataPath())
                .centerCrop()
                .placeholder(R.mipmap.zzz_no_image_available)
                .into(thumbnail);

        if (mii.getMimeType().equalsIgnoreCase("video/mp4") || mii.getMimeType().equalsIgnoreCase("video/flv")) {
            btnPlay.setVisibility(View.VISIBLE);
        } else {
            btnPlay.setVisibility(View.GONE);
        }

        return view;
    }

    public void addMediaItem(MediaItemInfo mii) {
        mMediaItemList.add(mii);
        Collections.sort(mMediaItemList, Collections.reverseOrder());
        notifyDataSetChanged();
    }

    public void removeItem(MediaItemInfo mii) {
        mMediaItemList.remove(mii);
        notifyDataSetChanged();
    }

    public void populateMediaItems(Map<String, MediaItemInfo> mediaItemList) {
        mMediaItemList.clear();

        String[] mimeTypeList = {
            AppConstants.MIME_TYPE_IMAGE_GIF, AppConstants.MIME_TYPE_IMAGE_JPEG, AppConstants.MIME_TYPE_IMAGE_JPG,
            AppConstants.MIME_TYPE_IMAGE_PNG, AppConstants.MIME_TYPE_VIDEO_MP4
        };

        Set set = mediaItemList.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, MediaItemInfo> mapKey = (Map.Entry<String, MediaItemInfo>) iterator.next();
            MediaItemInfo mii = mapKey.getValue();

            if (Arrays.asList(mimeTypeList).contains(mii.getMimeType())) {
                mMediaItemList.add(mii);
            }
        }

        Collections.sort(mMediaItemList);
        notifyDataSetChanged();
    }
}
