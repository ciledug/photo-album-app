package online.togie.photoalbumapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import online.togie.photoalbumapp.objects.MediaItemInfo;

public class VideoDetailActivity extends AppCompatActivity {

    private final String TAG = ImageDetailActivity.class.getSimpleName();

    private VideoView mDetailVideo;
    private MediaController mMediaController;

    private boolean mIsVideoPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        MediaItemInfo mii = i.getParcelableExtra(getString(R.string.intent_extra_selected_media_item));

        mDetailVideo = findViewById(R.id.detail_video_view);

        mMediaController = new MediaController(VideoDetailActivity.this);
        mMediaController.setAnchorView(mDetailVideo);
        mDetailVideo.setMediaController(mMediaController);
        mDetailVideo.setVideoPath(mii.getDataPath());

        mDetailVideo.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mMediaController.isShowing()) {
            mMediaController.hide();
        }

        if (mDetailVideo.isPlaying()) {
            mDetailVideo.stopPlayback();
        }

        finish();
    }
}
