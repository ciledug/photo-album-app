package online.togie.photoalbumapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import online.togie.photoalbumapp.R;
import online.togie.photoalbumapp.utils.AppConstants;

public class DialogCameraChoicesFragment extends BottomSheetDialogFragment {

    public interface DialogCameraChoicesFragmentListener {
        void onCameraChoiceClicked(CameraChoiceInfo cci);
    }

    private DialogCameraChoicesFragmentListener mListener;
    private Context mContext;

    public DialogCameraChoicesFragment(Context context, DialogCameraChoicesFragmentListener listener) {
        mContext = context;
        mListener = listener;
    }

    public static DialogCameraChoicesFragment newInstance(Context context, DialogCameraChoicesFragmentListener listener) {
        DialogCameraChoicesFragment fragment = new DialogCameraChoicesFragment(context, listener);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog_camera_choices, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(new DialogCameraChoicesAdapter());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }


    private class DialogCameraChoicesViewHolder extends RecyclerView.ViewHolder {

        ImageView mChoiceIcon;
//        TextView mChoiceTitle;

        DialogCameraChoicesViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_camera_choice, parent, false));
            mChoiceIcon = itemView.findViewById(R.id.item_camera_choice_thumbnail);
//            mChoiceTitle = itemView.findViewById(R.id.item_camera_choice_title);
        }
    }

    private class DialogCameraChoicesAdapter extends RecyclerView.Adapter<DialogCameraChoicesViewHolder> {

        private List<CameraChoiceInfo> mChoicesList = new ArrayList<>();

        public DialogCameraChoicesAdapter() {
            mChoicesList.add(new CameraChoiceInfo(android.R.drawable.ic_menu_camera, getString(R.string.camera_choice_picture), AppConstants.INTENT_TAKE_PHOTO_REQUEST));
            mChoicesList.add(new CameraChoiceInfo(android.R.drawable.presence_video_online, getString(R.string.camera_choice_video), AppConstants.INTENT_TAKE_VIDEO_REQUEST));
        }

        @Override
        public DialogCameraChoicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new DialogCameraChoicesViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(DialogCameraChoicesViewHolder holder, int position) {
            CameraChoiceInfo cci = mChoicesList.get(position);

//            holder.mChoiceTitle.setText(cci.getChoiceTitle());
            holder.mChoiceIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onCameraChoiceClicked(cci);
                }
            });

            Glide.with(mContext)
                    .load(cci.getChoiceDrawable())
                    .centerCrop()
                    .placeholder(R.mipmap.zzz_no_image_available)
                    .into(holder.mChoiceIcon);
        }

        @Override
        public int getItemCount() {
            return mChoicesList.size();
        }
    }

    public class CameraChoiceInfo {

        private int mChoiceDrawable;
        private StringBuilder mChoiceTitle = new StringBuilder("");
        private int mIntentTakeRequest = 0;

        public CameraChoiceInfo() {
        }

        public CameraChoiceInfo(int choiceDrawable, String choiceTitle, int intentTakeRequest) {
            mChoiceDrawable = choiceDrawable;
            mChoiceTitle.replace(0, mChoiceTitle.length(), choiceTitle);
            mIntentTakeRequest = intentTakeRequest;
        }

        public int getChoiceDrawable() {
            return mChoiceDrawable;
        }

        public void setChoiceDrawable(int choiceDrawable) {
            mChoiceDrawable = choiceDrawable;
        }

        public String getChoiceTitle() {
            return mChoiceTitle.toString();
        }

        public void setChoiceTitle(String choiceTitle) {
            mChoiceTitle.replace(0, mChoiceTitle.length(), choiceTitle);
        }

        public int getIntentTakeRequest() {
            return mIntentTakeRequest;
        }

        public void setIntentTakeRequest(int intentTakeRequest) {
            mIntentTakeRequest = intentTakeRequest;
        }
    }

}
