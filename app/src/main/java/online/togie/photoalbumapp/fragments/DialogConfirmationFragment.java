package online.togie.photoalbumapp.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;

import online.togie.photoalbumapp.R;
import online.togie.photoalbumapp.objects.MediaItemInfo;

public class DialogConfirmationFragment extends DialogFragment {

    public interface DialogConfirmationFragmentListener {
        void onConfirmationOkButtonClicked(MediaItemInfo mii, int sourceFromType);
        void onConfirmationCancelButtonClicked();
    }

    private DialogConfirmationFragmentListener mListener;
    private MediaItemInfo mMediaItemInfo;
    private int mSourceFromType = 0;
    private boolean mIsWithImage = false;
    private StringBuilder mDialogMessage = new StringBuilder("");

    public DialogConfirmationFragment(DialogConfirmationFragmentListener listener, MediaItemInfo mii, int sourceFromType, boolean isWithImage, String message) {
        mListener = listener;
        mMediaItemInfo = mii;
        mSourceFromType = sourceFromType;
        mIsWithImage = isWithImage;
        mDialogMessage.replace(0, mDialogMessage.length(), message);
    }

    public static DialogConfirmationFragment newInstance(DialogConfirmationFragmentListener listener, MediaItemInfo mii, int sourceFromType, boolean isWithImage, String message) {
        return new DialogConfirmationFragment(listener, mii, sourceFromType, isWithImage, message);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        View v = inflater.inflate(R.layout.fragment_dialog_confirmation, container, false);
        AppCompatButton yesButton = v.findViewById(R.id.dialog_confirmation_button_yes);
        AppCompatButton cancelButton = v.findViewById(R.id.dialog_confirmation_button_cancel);
        ImageView mediaThumbnail = v.findViewById(R.id.dialog_confirmation_media_thumbnail);
        AppCompatTextView dialogMessage = v.findViewById(R.id.dialog_confirmation_message);

        dialogMessage.setText(mDialogMessage.toString());

        if (mIsWithImage) {
            Glide.with(this)
                    .load(mMediaItemInfo.getDataPath())
                    .placeholder(R.mipmap.zzz_no_image_available)
                    .centerCrop()
                    .into(mediaThumbnail);
        } else {
            mediaThumbnail.setVisibility(View.GONE);
        }

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onConfirmationOkButtonClicked(mMediaItemInfo, mSourceFromType);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onConfirmationCancelButtonClicked();
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
