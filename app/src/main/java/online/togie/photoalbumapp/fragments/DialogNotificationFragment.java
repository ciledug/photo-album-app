package online.togie.photoalbumapp.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import online.togie.photoalbumapp.R;

public class DialogNotificationFragment extends DialogFragment {

    public interface DialogNotificationFragmentListener {
        void onNotificationButtonClicked();
    }

    private DialogNotificationFragmentListener mListener;
    private StringBuilder mDialogMessageText = new StringBuilder("");

    public DialogNotificationFragment(DialogNotificationFragmentListener listener, String message) {
        mListener = listener;
        mDialogMessageText.replace(0, mDialogMessageText.length(), message);
    }

    public static DialogNotificationFragment newInstance(DialogNotificationFragmentListener listener, String message) {
        DialogNotificationFragment fragment = new DialogNotificationFragment(listener, message);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        View v = inflater.inflate(R.layout.fragment_dialog_permission, container, false);

        AppCompatTextView dialogMessage = v.findViewById(R.id.dialog_permission_message);
        dialogMessage.setText(mDialogMessageText.toString());

        AppCompatButton okButton = v.findViewById(R.id.dialog_permission_button_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onNotificationButtonClicked();
            }
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
